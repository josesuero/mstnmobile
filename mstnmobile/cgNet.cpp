//
//  cgNet.cpp
//  mstnmobile
//
//  Created by Jose Suero on 11/4/15.
//  Copyright © 2015 MSTN Media. All rights reserved.
//

#include "cgNet.hpp"

using curl::curl_easy;
using curl::make_option;

using namespace mstn;

cgNet::cgNet(){
    
}
cgNet::~cgNet(){
    
}

size_t cgNet::handle(char * data, size_t size, size_t nmemb, void * p)
{
    return static_cast<cgNet*>(p)->handle_impl(data, size, nmemb);
}

size_t cgNet::handle_impl(char* data, size_t size, size_t nmemb)
{
    content_.append(data, size * nmemb);
    return size * nmemb;
}
char* cgNet::GetStringVERB(enum tagVERB index){ return gs_VERB [index]; }

string cgNet::escape(string component){
    easy.escape(component);
    return component;
}
string cgNet::unescape(string component){
    easy.unescape(component);
    return component;
}

string cgNet::perform(string url, VERB v, const char* data){
    cgNet net;
    easy.add<CURLOPT_URL>(url.c_str());
    easy.add<CURLOPT_FOLLOWLOCATION>(1L);
    easy.add<CURLOPT_WRITEFUNCTION>(handle);
    easy.add<CURLOPT_WRITEDATA>(&net);
    
    if (v != GET){
        easy.add<CURLOPT_POSTFIELDS>(data);
        easy.add<CURLOPT_CUSTOMREQUEST>(GetStringVERB(v));
    }
    
    try {
        easy.perform();
        return net.content_;
    }
    catch (curl_easy_exception error) {
        string errorText{};
        for (pair<string,string> a:error.get_traceback()){
            errorText.append(a.first).append("=").append(a.second).append("\n");
        //
        }
        throw netException(errorText.c_str());
    }
}

string cgNet::get(string url){
    return perform(url, VERB::GET, (char*)"");
}

string cgNet::post(string url, char* data){
    return perform(url, VERB::POST,data);
}


string cgNet::put(string url, char* data){
    return perform(url, VERB::PUT,data);
}


string cgNet::del(string url, char* data){
    return perform(url, VERB::DELETE,data);
}

string cgNet::json2query(cgObject* data){
    string qs{};
    for (string key:data->keys()){
        char* keyChar = const_cast<char*>(key.c_str());
        string dataobj{};
        if (data->isArray(keyChar)|| data->isObject(keyChar)){
            dataobj = data->get(keyChar).toString();
        } else {
            dataobj = data->getString(keyChar);
        }
        
        qs.append(key).append("=").append(escape(dataobj)).append("&");
    }
    return qs;
}

string cgNet::get(string url, cgObject* data){
    return get(url.append("?").append(json2query(data)));
}
string cgNet::post(string url, cgObject* data){
    return post(url, const_cast<char*>(json2query(data).c_str()));
}
string cgNet::put(string url, cgObject* data){
    return put(url, const_cast<char*>(json2query(data).c_str()));
}
string cgNet::del(string url, cgObject* data){
    return del(url, const_cast<char*>(json2query(data).c_str()));
}
