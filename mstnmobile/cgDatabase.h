//
//  Database.hpp
//  mstnmobile
//
//  Created by Jose Suero on 11/1/15.
//  Copyright © 2015 MSTN Media. All rights reserved.
//

#ifndef cgDatabase_h
#define cgDatabase_h

#include <iostream>
#include <sqlite3.h>
#include "cgObject.h"
#include "cgException.h"

using namespace std;


namespace mstn{
    class cgDatabase{
    private:
        char *name;
        sqlite3 *db;
        char *zErrMsg = 0;
    public:
        cgDatabase();
        cgDatabase(string dbname);
        cgDatabase(char *dbname);
        ~cgDatabase();
        
        void close();
        
        int createDB(char *name);
        int createTable(string name);
        
        cgObject* query(const char* sql);
        int noquery(const char *sql);
        int noquery(const char *sql, const vector<string> values);
        
        int startTransaction();
        int endTransaction();
        
        cgObject& select(string table,cgObject data);
        int insertData(string table, cgObject* data);
        int updateData(string table, cgObject* data, string keyfield);
        int deleteData(string table, cgObject* data, string keyfield);
        
        string error();
    };
}
#endif /* cgDatabase_h */
