//
//  cgNet.hpp
//  mstnmobile
//
//  Created by Jose Suero on 11/4/15.
//  Copyright © 2015 MSTN Media. All rights reserved.
//

#ifndef cgNet_hpp
#define cgNet_hpp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "curl_easy.h"
#include "curl_option.h"

#include "cgObject.h"
#include "cgException.h"

using curl::curl_easy;
using curl::make_option;

namespace mstn{
    
    class cgNet{
    private:
        string content_;
        static size_t handle(char * data, size_t size, size_t nmemb, void * p);
        size_t handle_impl(char * data, size_t size, size_t nmemb);
        curl_easy easy;
        
        
    public:
        string json2query(cgObject* data);
        typedef enum tagVERB
        {
            GET,
            POST,
            PUT,
            DELETE
        }
        VERB;
        
        char* GetStringVERB(enum tagVERB index);
        char* gs_VERB [4] =
        {
            (char*)"GET",
            (char*)"POST",
            (char*)"PUT",
            (char*)"DELETE"
        };

        cgNet();
        ~cgNet();
        
        string perform(string url, VERB v, const char* data);
        
        string get(string url);
        string post(string url, char* data);
        string put(string url, char* data);
        string del(string url, char* data);
        
        string get(string url, cgObject* data);
        string post(string url, cgObject* data);
        string put(string url, cgObject* data);
        string del(string url, cgObject* data);
        
        string escape(string component);
        string unescape(string component);
    };
}

#endif /* cgNet_hpp */
