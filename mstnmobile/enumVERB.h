//
//  VERBenum.h
//  mstnmobile
//
//  Created by Jose Suero on 11/5/15.
//  Copyright © 2015 MSTN Media. All rights reserved.
//

#if ( !defined(DAYS_H) || defined(GENERATE_ENUM_STRINGS) )

#if (!defined(GENERATE_ENUM_STRINGS))
#define DAYS_H
#endif

#include "EnumToStrings.hpp"

///////////////////////////////
// The enum declaration
///////////////////////////////
BEGIN_ENUM(VERB)
{
    DECL_ENUM_ELEMENT(GET),
    DECL_ENUM_ELEMENT(POST),
    DECL_ENUM_ELEMENT(PUT),
    DECL_ENUM_ELEMENT(DELETE)
}
END_ENUM(VERB)

#endif // (!defined(DAYS_H) || defined(GENERATE_ENUM_STRINGS))