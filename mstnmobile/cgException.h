//
//  cgException.hpp
//  mstnmobile
//
//  Created by Jose Suero on 11/1/15.
//  Copyright © 2015 MSTN Media. All rights reserved.
//

#ifndef cgException_hpp
#define cgException_hpp

#include <iostream>
#include <exception>

using namespace std;
namespace mstn {
    class cgException : public exception{
    private:
        string errorMsg;
    public:
        cgException() = default;
        cgException(const char* error):errorMsg(error){
        }
        string what ()
        {
            return errorMsg;
        }
    };
    
    class dbException :public cgException{
    public:
        dbException() = default;
        dbException(const char* errorMsg):cgException(errorMsg){};
    };
    
    class netException :public cgException{
    public:
        netException() = default;
        netException(const char* errorMsg):cgException(errorMsg){};
    };
}
#endif /* cgException_hpp */
