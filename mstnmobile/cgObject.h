//
//  cgRowset.hpp
//  mstnmobile
//
//  Created by Jose Suero on 11/1/15.
//  Copyright © 2015 MSTN Media. All rights reserved.
//

#ifndef cgObject_hpp
#define cgObject_hpp

#include <iostream>
#include <vector>
#include "cgException.h"

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/encodings.h"

using namespace std;
using namespace rapidjson;

namespace mstn{
    
    enum cgObjectType{ARRAY,OBJECT};
    
    class cgObject{
    private:
        Document* document = nullptr;
        cgObjectType type = cgObjectType::OBJECT;
        Value value;
        bool child = false;
        
        GenericValue<UTF8<char>, MemoryPoolAllocator<CrtAllocator>> getValue(const char* name);
        
    public:
        class iterator;
        cgObject();
        cgObject(Document* document);
        cgObject(Document* document, cgObjectType objType);
        cgObject(const cgObject& other);
        cgObject(Document* document,Value value);
        cgObject(Document* document,Value value, cgObjectType objType);
        
        cgObject(string json);
        
        ~cgObject();
        
        string toString() const;
        void close();
        
        cgObject* newArray();
        cgObject* newObject();
        
        cgObject addArray(string name, cgObject array);
        cgObject* insert(const char* name, const char* content);
        cgObject* insert(const char* name, cgObject* content);
        cgObject* insert(cgObject* content);
        
        friend ostream &operator<< (ostream &out, const cgObject &obj){
            out << obj.toString();
            return out;
        }
        
        string getString(char* n);
        int getInt(char* n);
        bool getBool(char* n);
        double getDouble(char* n);
        cgObject get(char* n);

        string getString(int n);
        int getInt(int n);
        bool getBool(int n);
        double getDouble(int n);
        cgObject get(int n);

        bool isString(char* n);
        bool isInt(char* n);
        bool isBool(char* n);
        bool isDouble(char* n);
        bool isObject(char* n);
        bool isArray(char* n);

        bool isString(int n);
        bool isInt(int n);
        bool isBool(int n);
        bool isDouble(int n);
        bool isObject(int n);
        bool isArray(int n);
        
        vector<char*> keys();
        
        void parse(string json);
        /*
        iterator Begin(){
            return iterator(value.MemberBegin()->name.GetString(),*this);
        }
        iterator End(){
            return iterator(,*this);
        }
        */
    };
    
    class cgObject::iterator: public Value::MemberIterator{
        char* m_pos;
        cgObject &m_object;
    public:
        iterator(char* pos, cgObject &obj):m_pos(pos),m_object(obj){
        }
        
        iterator& operator++(int){
            m_pos++;
            return *this;
        }
        iterator& operator++(){
            m_pos++;
            return *this;
        }
        
        const int operator*(){
            return 1;//m_object.get(m_pos);
        }
        bool operator!= (const iterator &other) const{
            return m_pos != other.m_pos;
        }
    };
};


#endif /* cgObject_hpp */
