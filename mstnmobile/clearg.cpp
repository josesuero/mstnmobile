//
//  clearg.cpp
//  mstnmobile
//
//  Created by Jose Suero on 11/8/15.
//  Copyright © 2015 MSTN Media. All rights reserved.
//

#include "clearg.hpp"

clearg::clearg(string uri, string schema, string token):uri(uri),schema(schema),token(token){
    
};

cgObject clearg::getResource(string uri, cgObject* queryParams){
    cgNet resource;
    string content = resource.post(uri, queryParams);
    return cgObject(content);
}

string clearg::getToken(){
    return token;
}

string clearg::login(string username, string password, string schema){
    return login(username,password,schema,"");
}

string clearg::login(string username, string password, string schema, string token){
    cgObject* queryParams = new cgObject();
    queryParams->insert((char*)"username", username.c_str());
    queryParams->insert((char*)"password", password.c_str());
    queryParams->insert((char*)"schema", schema.c_str());
    queryParams->insert((char*)"token", token.c_str());
    cgObject data = getResource(this->uri + "login", queryParams);
    this->token = data.getString((char*)"content");
    return this->token;
}

