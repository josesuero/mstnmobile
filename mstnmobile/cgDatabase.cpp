//
//  Database.cpp
//  mstnmobile
//
//  Created by Jose Suero on 11/1/15.
//  Copyright © 2015 MSTN Media. All rights reserved.
//

#include <exception>
#include "cgCore.hpp"
#include "cgDatabase.h"


using namespace mstn;

cgDatabase::cgDatabase():db(nullptr),name(NULL){
    
};

cgDatabase::cgDatabase(char *dbname):db(nullptr),name(dbname){
    createDB(dbname);
};

cgDatabase::~cgDatabase(){
    if (name != NULL){
        sqlite3_close(db);
    }
};

void cgDatabase::close(){
    sqlite3_close (db);
}


int cgDatabase::createDB(char *name){
    if (sqlite3_open(name, &db) != SQLITE_OK) {
        char* errormsg = (char*)sqlite3_errmsg(db);
        sqlite3_close(db);
        
        throw dbException(errormsg);
    }
    return 0;
};

cgObject* cgDatabase::query(const char* sql){

    cgObject* document =  new cgObject();
    auto data = document->newArray();
    sqlite3_stmt *statement;
    
    int rc = sqlite3_prepare(db, sql, -1, &statement, 0);
    if (rc != SQLITE_OK) {
        char* error = (char*)sqlite3_errmsg(db);
        throw dbException(error);
        
    } else {
        int ctotal = sqlite3_column_count(statement);
        int res = 0;
        
        while (1) {
            res = sqlite3_step(statement);
            
            cgObject* row = document->newObject();
            
            if (res == SQLITE_ROW) {
                for (int i = 0; i < ctotal; i++) {
                    auto n = (char*) sqlite3_column_name(statement, i);
                    auto v = (char*) sqlite3_column_text(statement, i);
                    row->insert(n,v);
/*                    row.AddMember(
                                  GenericValue<UTF8<char>,
                                  MemoryPoolAllocator<CrtAllocator>>(n,
                                                                     allocator),
                                  GenericValue<UTF8<char>,
                                  MemoryPoolAllocator<CrtAllocator>>(v,
                                                                     allocator), allocator);
  */
                }
                
            }
            
            if (res == SQLITE_DONE || res == SQLITE_ERROR) {
                break;
            }
            data->insert(row);
            delete row;
            //data.PushBack(row, allocator);
        }
    }
    document->insert((char*)"data",data);
    //document->AddMember("data", data, allocator);
    delete data;
    
    sqlite3_finalize(statement);

    return document;
}

int cgDatabase::noquery(const char *sql, const vector<string> values){
    
    sqlite3_stmt *stmt;
    const char *pzTest;
    int rc = sqlite3_prepare(db, sql, static_cast<unsigned int>(strlen(sql)), &stmt, &pzTest);
    
    if( rc  != SQLITE_OK) {
        char* errormsg = (char*)sqlite3_errmsg(db);
        sqlite3_close(db);
        
        throw dbException(errormsg);
    }
    
    for(int i = 0;i <  values.size();i++){
        sqlite3_bind_text(stmt, i + 1, values[i].c_str(), static_cast<unsigned int>(strlen(values[i].c_str())), 0);
    }
    
    // commit
    sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    return 0;
}
int cgDatabase::noquery(const char *sql){
    int rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        sqlite3_free(zErrMsg);
        throw dbException(zErrMsg);
    }
    return 0;
}

int cgDatabase::insertData(string table, cgObject* data){
    vector<string> values;
    vector<char*> keys = data->keys();
    
    string sql;
    sql += "insert into ";
    sql += table;
    sql += "(";
    string keystr;
    for (auto key:keys){
        keystr += ",";
        keystr += key;
    }
    sql += keystr.substr(1);
    
    sql += ")";
    
    sql += " values(";
    
    keystr = "";
    values.clear();

    for (auto key:keys){
        keystr += ",?";
        
        values.push_back(data->getString(key));
        
        //keystr += data->getString(key);
    }
    
    sql += keystr.substr(1);
    
    sql += ");";
    
    noquery(sql.c_str(),values);
    
    return 0;
}

int cgDatabase::updateData(string table, cgObject* data, string keyfield){
    vector<string> values;
    
    vector<char*> keys = data->keys();
    keys.erase(find(keys.begin(),keys.end(),keyfield));
    
    string sql;
    sql += "update ";
    sql += table;
    sql += " set ";
    string keystr;
    for (auto key:keys){
        keystr += ",";
        keystr += key;
        keystr += "=?";
        values.push_back(data->getString(key));
    }
    sql += keystr.substr(1);
    sql += " where ";
    sql += keyfield;
    sql += " = ?";
    values.push_back(data->getString(const_cast<char*>(keyfield.c_str())));
    sql += ";";
    
    noquery(sql.c_str(),values);
    
    return 0;
}

int cgDatabase::deleteData(string table, cgObject* data, string keyfield){
    vector<string> values;
    vector<char*> keys = data->keys();
    keys.erase(find(keys.begin(),keys.end(),keyfield));
    
    string sql;
    sql += "delete from ";
    sql += table;
    sql += " where ";
    sql += keyfield;
    sql += " = ?;";
    values.push_back(data->getString(const_cast<char*>(keyfield.c_str())));
    
    noquery(sql.c_str(),values);
    
    return 0;
}