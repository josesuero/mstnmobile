//
//  cgRowset.cpp
//  mstnmobile
//
//  Created by Jose Suero on 11/1/15.
//  Copyright © 2015 MSTN Media. All rights reserved.
//

#include "cgObject.h"

using namespace mstn;

cgObject::cgObject(){
    document = new Document();
    //value = new Value();
    value.SetObject();
};

cgObject::cgObject(Document* otherDocument){
    document = otherDocument;
    //value = new Value();
    child = true;
}

cgObject::cgObject(Document* otherDocument, cgObjectType objtype){
    document = otherDocument;
    type = objtype;
    //value = new Value();
    child = true;
}


cgObject::cgObject(const cgObject& other){
    document = other.document;
    this->value.CopyFrom(other.value, document->GetAllocator());
}

cgObject::cgObject(string json):cgObject(){
    parse(json);
}

cgObject::~cgObject(){
    close();
}

void cgObject::close(){
    if (!child)
        delete document;
    //delete value;
}

string cgObject::toString() const{
    StringBuffer strbuf;
    Writer<StringBuffer> writer(strbuf);
    value.Accept(writer);
    return strbuf.GetString();
}

cgObject* cgObject::newArray(){
    cgObject* newvalue = new cgObject(document,cgObjectType::ARRAY);
    newvalue->value.SetArray();
    return newvalue;
}

cgObject* cgObject::newObject(){
    cgObject* newvalue = new cgObject(document);
    newvalue->value.SetObject();
    return newvalue;
}

GenericValue<UTF8<char>, MemoryPoolAllocator<CrtAllocator>> cgObject::getValue(const char* name){
    return GenericValue<UTF8<char>, MemoryPoolAllocator<CrtAllocator>>(name, document->GetAllocator());
}


cgObject* cgObject::insert(const char* name, const char* content){
    Document::AllocatorType& allocator = document->GetAllocator();
    value.AddMember(getValue(name), getValue(content),allocator);
    return this;
}

cgObject* cgObject::insert(const char* name, cgObject* content){
    Document::AllocatorType& allocator = document->GetAllocator();
    value.AddMember(getValue(name), content->value,allocator);
    return this;
}

cgObject* cgObject::insert(cgObject* content){
    value.PushBack(content->value,document->GetAllocator());
    return this;
}

string cgObject::getString(char* n){
    return value[n].GetString();
}
int cgObject::getInt(char* n){
    return value[n].GetInt();
}
bool cgObject::getBool(char* n){
    return value[n].GetBool();
}
double cgObject::getDouble(char* n){
    return value[n].GetDouble();
}

cgObject cgObject::get(char* n){
    cgObject temp(document);
    temp.value = value[n];
    return temp;
}

string cgObject::getString(int n){
    return value[n].GetString();
}
int cgObject::getInt(int n){
    return value[n].GetInt();
}
bool cgObject::getBool(int n){
    return value[n].GetBool();
}
double cgObject::getDouble(int n){
    return value[n].GetDouble();
}

cgObject cgObject::get(int n){
    cgObject temp(document);
    temp.value = value[n];
    return temp;
}


bool cgObject::isString(char* n){
    return value[n].IsString();
}
bool cgObject::isInt(char* n){
    return value[n].IsInt();
}
bool cgObject::isBool(char* n){
    return value[n].IsBool();
}
bool cgObject::isDouble(char* n){
    return value[n].IsDouble();
}
bool cgObject::isObject(char* n){
    return value[n].IsObject();
}

bool cgObject::isArray(char* n){
    return value[n].IsArray();
}

bool cgObject::isString(int n){
    return value[n].IsString();
}
bool cgObject::isInt(int n){
    return value[n].IsInt();
}
bool cgObject::isBool(int n){
    return value[n].IsBool();
}
bool cgObject::isDouble(int n){
    return value[n].IsDouble();
}
bool cgObject::isObject(int n){
    return value[n].IsObject();
}

bool cgObject::isArray(int n){
    return value[n].IsArray();
}
vector<char*> cgObject::keys(){
    vector<char*> all;
    for(Value::MemberIterator key = value.MemberBegin();key != value.MemberEnd();key++){
        char* keyname = const_cast<char*>(key->name.GetString());
        all.push_back(keyname);
    }
    return all;
}

void cgObject::parse(string json){
    Document d;
    
    value.CopyFrom(d.Parse(json.c_str()),document->GetAllocator());
    if (value.IsArray()){
        type = cgObjectType::ARRAY;
    } else if (value.IsObject()){
        type = cgObjectType::OBJECT;
    }
}