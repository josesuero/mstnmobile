//
//  clearg.hpp
//  mstnmobile
//
//  Created by Jose Suero on 11/8/15.
//  Copyright © 2015 MSTN Media. All rights reserved.
//

#ifndef clearg_hpp
#define clearg_hpp

#include <stdio.h>
#include "cgCore.hpp"

using namespace std;
using namespace mstn;


class clearg{
private:
    string token;
    string uri;
    string schema;
public:
    clearg(string uri):clearg(uri,""){}
    clearg(string uri, string schema):clearg(uri, schema,""){}
    clearg(string uri, string schema, string token);
    
    cgObject getResource(string uri, cgObject* data);
    
    string login(string username, string password, string schema);
    string login(string username, string password, string schema, string token);
    
    string logout(string token);
    
    bool isValid(string token);
    
    bool authorize(string type, string name, string action);
    
    cgObject senddata(string command, string data);
    cgObject senddata(string command, string schema, string data);
    
    cgObject getJsonData(string command, string data);
    cgObject getJsonData(string command, string schema, string data);
    
    string getScreen(string screenname);
    
    
    
    string getToken();
    void setToken(string token);
    
};



#endif /* clearg_hpp */
